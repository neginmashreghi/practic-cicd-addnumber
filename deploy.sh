echo "Deploy script started"
cd /root/addnumber
git pull origin master 
# In this project, we don't need to rerun the project after we changed the code 
# because we are running an express server in the background of our staging server 
# and our express server will automatically rerun after all the update. 
echo "Deploy script finished execution"